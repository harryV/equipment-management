package com.lee.web.controller;

import com.lee.web.bean.Student;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lizhe
 */
@Controller
public class LoginController {

    @RequestMapping(value = "/login")
    public String homePage() {
        return "customLogin";
    }

    @RequestMapping(value = "/secure/studentDetail")
    public String studentDetail(Model model) {

        List<Student> list = new ArrayList<>();
        {
            list.add(new Student(1, "Ram"));
            list.add(new Student(2, "Shyam"));
            list.add(new Student(3, "Rahim"));
        }

        model.addAttribute("students", list);
        return "student";
    }
}
