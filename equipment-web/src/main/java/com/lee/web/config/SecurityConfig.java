package com.lee.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

/**
 * Created by 喆 on 2015/11/18.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    // 查询用户和角色
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource).passwordEncoder(passwordEncoder())
                .usersByUsernameQuery("SELECT LOWER(username) username,password,enabled FROM users WHERE LOWER(username) = ?")
                .authoritiesByUsernameQuery(
                        "SELECT LOWER(u.username) username,a.authority FROM authorities a,users u WHERE LOWER(a.username)=LOWER(u.username) AND LOWER(a.username) = ?");
        auth.eraseCredentials(true);
    }

//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication().withUser("ramesh").password("ram123").roles("ADMIN");
//        auth.inMemoryAuthentication().withUser("mahesh").password("mah123").roles("USER");
//    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests()
//                .antMatchers("/static/**").permitAll().and().authorizeRequests().antMatchers("/login").permitAll().anyRequest()
//                .fullyAuthenticated().and().formLogin().loginPage("/login")
//                .failureUrl("/login?error").and().logout()
//                .logoutRequestMatcher(new AntPathRequestMatcher("/logout")).deleteCookies("JSESSIONID").and()
//                .exceptionHandling().accessDeniedPage("/access?error");
        http.authorizeRequests().
                antMatchers("/static/**").access("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')").
                and().formLogin().  //login configuration
                loginPage("/app/login").
                loginProcessingUrl("/appLogin").
                usernameParameter("app_username").
                passwordParameter("app_password").
                defaultSuccessUrl("/app/secure/studentDetail").
                and().logout().    //logout configuration
                logoutUrl("/appLogout").
                logoutSuccessUrl("/app/login");
    }


    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}