package com.lee.web.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author lizhe
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}